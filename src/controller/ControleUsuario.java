package controller;
import java.util.ArrayList;
import model.*;

public class ControleUsuario {
	ArrayList<Medico> listaMedicos;
        ArrayList<Paciente> listaPacientes;
	
	public ControleUsuario(){
		listaMedicos = new ArrayList<Medico>();
	}
	public void adicionar(Medico umMedico){
		
		listaMedicos.add(umMedico);
		
	}
        public void adicionarP(Paciente umPaciente){
		
		listaPacientes.add(umPaciente);
		
	}
        
	public void remover(Medico umMedico){
		
		listaMedicos.remove(umMedico);
	
	}
        public void removerP(Paciente umPaciente){
		
		listaPacientes.remove(umPaciente);
	
	}
	public Medico buscar(String umNome){
		
		for (Medico umMedico: listaMedicos){
			if (umMedico.getNome().equalsIgnoreCase(umNome)){
				return umMedico;
			}
		}
		return null;
	}
        public Paciente buscarP(String umNome){
		
		for (Paciente umPaciente: listaPacientes){
			if (umPaciente.getNome().equalsIgnoreCase(umNome)){
				return umPaciente;
			}
		}
		return null;
	}
}	